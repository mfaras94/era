// Styles
// import '@fortawesome/fontawesome-free/css/all.css'
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import 'vuetify/dist/vuetify.min.css'
// import { aliases, fa } from 'vuetify/iconsets/fa'

// Vuetify
import { createVuetify } from 'vuetify'

export default createVuetify(
 {
  theme: {
    options: {
      customProperties: true,
    },
    fontFamily: 'Poppins, sans-serif',
    // icons: {
    //   defaultSet: 'fa',
    //   aliases,
    //   sets: {
    //     fa,
    //   },
    // },
  },
 }
)
