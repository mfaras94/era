/**
 * plugins/webfontloader.js
 *
 * webfontloader documentation: https://github.com/typekit/webfontloader
 */

export async function loadFonts () {
  const webFontLoader = await import(/* webpackChunkName: "webfontloader" */'webfontloader')

  webFontLoader.load({
    google: {
      families: ['Poppins:wght@100;200;300;400;500;600;700;900&display=swap'],
    },
    active: function() {
      console.log('Font loaded');
    },
    inactive: function() {
      console.log('Font failed to load');
    }
  })
}
